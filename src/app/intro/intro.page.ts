import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {

  slideOpts = {
    initialSlide: 1,
    speed: 400,
    pagination: {
      el: '.swiper-pagination',
      
    }
  };

  constructor(private router: Router) { }
  navigate(){
    this.router.navigate(['/signup'])
  }


  ngOnInit() {
  }

}
