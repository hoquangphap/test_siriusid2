import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-signup1',
  templateUrl: './signup1.page.html',
  styleUrls: ['./signup1.page.scss'],
})
export class Signup1Page implements OnInit {

  constructor(private router: Router) { }
  navigate(){
    this.router.navigate(['/tabs'])
  }


  ngOnInit() {
  }

}
