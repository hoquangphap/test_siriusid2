import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'intro', loadChildren: './intro/intro.module#IntroPageModule' },
  { path: 'terms', loadChildren: './terms/terms.module#TermsPageModule' },
  { path: 'policy', loadChildren: './policy/policy.module#PolicyPageModule' },
  { path: 'recovery', loadChildren: './recovery/recovery.module#RecoveryPageModule' },
  { path: 'recovery1', loadChildren: './recovery1/recovery1.module#Recovery1PageModule' },
  { path: 'recovery2', loadChildren: './recovery2/recovery2.module#Recovery2PageModule' },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  { path: 'signup1', loadChildren: './signup1/signup1.module#Signup1PageModule' },
  { path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule' },
  // { path: 'backup', loadChildren: './backup/backup.module#BackupPageModule' },
  // { path: 'nortification', loadChildren: './nortification/nortification.module#NortificationPageModule' },
  // { path: 'transaction-detail1', loadChildren: './transaction-detail1/transaction-detail1.module#TransactionDetail1PageModule' },
  // { path: 'transaction-detail', loadChildren: './transaction-detail/transaction-detail.module#TransactionDetailPageModule' },
  // { path: 'history', loadChildren: './history/history.module#HistoryPageModule' },
  // { path: 'camera', loadChildren: './camera/camera.module#CameraPageModule' },
  // { path: 'user1', loadChildren: './user1/user1.module#User1PageModule' },
  // { path: 'my-qrcode', loadChildren: './my-qrcode/my-qrcode.module#MyQRcodePageModule' },
  // { path: 'confirm1', loadChildren: './confirm1/confirm1.module#Confirm1PageModule' },
  // { path: 'confirm2', loadChildren: './confirm2/confirm2.module#Confirm2PageModule' },
  // { path: 'confirm', loadChildren: './confirm/confirm.module#ConfirmPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
