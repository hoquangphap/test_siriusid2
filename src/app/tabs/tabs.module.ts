import { NgModule } from '@angular/core';
import { CommonModule, ɵNgClassImplProvider__POST_R3__ } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';


import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children:
      [
        {
          path: 'main',
          children: [
            {path: '', loadChildren: '../main/main.module#MainPageModule'},
            {path: 'camera',loadChildren: '../camera/camera.module#CameraPageModule' },
            { path: 'confirm', loadChildren: '../confirm/confirm.module#ConfirmPageModule'},
            { path: 'confirm1', loadChildren: '../confirm1/confirm1.module#Confirm1PageModule'},
            { path: 'result', loadChildren: '../confirm2/confirm2.module#Confirm2PageModule'},
            {path: 'history', loadChildren: '../history/history.module#HistoryPageModule'},
            {path: 'transactionDetail', loadChildren: '../transaction-detail/transaction-detail.module#TransactionDetailPageModule'},
            {path: 'transactionDetail1', loadChildren: '../transaction-detail1/transaction-detail1.module#TransactionDetail1PageModule'},

        ]},

        { path: 'user', 
          children: [
            {path: '', loadChildren: '../user/user.module#UserPageModule'},
            {path: 'qr', loadChildren: '../my-qrcode/my-qrcode.module#MyQRcodePageModule'},
            {path: 'user1', loadChildren: '../user1/user1.module#User1PageModule'},
        ]},



        { path: 'setting',
        children: [
          {path: '',  loadChildren: '../setting/setting.module#SettingPageModule'},
          {path: 'nortification', loadChildren: '../nortification/nortification.module#NortificationPageModule' },
          {path: 'backup', loadChildren: './backup/backup.module#BackupPageModule' },          
        ]},

        {
          path: '',
          redirectTo: '/tabs/main',
          pathMatch: 'full'
        }
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
