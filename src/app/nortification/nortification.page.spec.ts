import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NortificationPage } from './nortification.page';

describe('NortificationPage', () => {
  let component: NortificationPage;
  let fixture: ComponentFixture<NortificationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NortificationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NortificationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
