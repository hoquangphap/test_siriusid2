import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Recovery2Page } from './recovery2.page';

describe('Recovery2Page', () => {
  let component: Recovery2Page;
  let fixture: ComponentFixture<Recovery2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Recovery2Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Recovery2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
