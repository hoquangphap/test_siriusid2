import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Recovery1Page } from './recovery1.page';

describe('Recovery1Page', () => {
  let component: Recovery1Page;
  let fixture: ComponentFixture<Recovery1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Recovery1Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Recovery1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
