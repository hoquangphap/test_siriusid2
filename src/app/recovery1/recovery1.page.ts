import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-recovery1',
  templateUrl: './recovery1.page.html',
  styleUrls: ['./recovery1.page.scss'],
})
export class Recovery1Page implements OnInit {

  

  constructor(private router: Router) { }
  navigate(){
    this.router.navigate(['/recovery2'])
  }

  ngOnInit() {
  }

}
