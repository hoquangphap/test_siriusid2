import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TransactionDetail1Page } from './transaction-detail1.page';

const routes: Routes = [
  {
    path: '',
    component: TransactionDetail1Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TransactionDetail1Page]
})
export class TransactionDetail1PageModule {}
