import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionDetail1Page } from './transaction-detail1.page';

describe('TransactionDetail1Page', () => {
  let component: TransactionDetail1Page;
  let fixture: ComponentFixture<TransactionDetail1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionDetail1Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionDetail1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
