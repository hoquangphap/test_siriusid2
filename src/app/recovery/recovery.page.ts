import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-recovery',
  templateUrl: './recovery.page.html',
  styleUrls: ['./recovery.page.scss'],
})
export class RecoveryPage implements OnInit {

  constructor(private router: Router) { }
  navigate(){
    this.router.navigate(['/recovery1'])
  }

  ngOnInit() {
  }

}
